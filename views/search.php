<?php
session_start();
  if(!isset($_SESSION['name'])){
     header("Location: login.php");
}

require "../vendor/autoload.php";
use App\Login;
use App\Contact;
use App\Database;


$contacts = Contact::search_contacts();

$search_key = isset($_GET['search']) ? trim($_GET['search']) : '';

$results = array();
if($search_key){
  foreach($contacts as $contact){
    if(stristr($contact['name'], $search_key)){
      $results[] = $contact;
    }
  }
}


$arr_size = sizeof($results);
if($arr_size>0){
  $_SESSION['success'] = $arr_size." results found.";
}
else{if($search_key) $_SESSION['error'] = "No results found.";}


$p_title = "Search";
?>

<!DOCTYPE html>
<html lang="en">

<?php include '../includes/header.php'; ?>

<body class="hold-transition sidebar-mini">
  <!-- Navbar -->
<?php include '../includes/navbar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-l-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Dashboard</li>
              <li class="breadcrumb-item active">Search</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6 offset-5">
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 offset-3">   
             <div class="card card-solid">
             <div class="card-body pb-4">   
              <form class="form-inline ml-3" action="" method="GET">
              <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search" required>
                <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
          </div> 
        </div>

        <div class="col-sm-6 offset-4">  
        <h4>
          <span class="text text-success"><?php 
          if ( isset($_SESSION['success']) ) {
          echo('<p>'.htmlentities($_SESSION['success'])."</p>\n");
          unset($_SESSION['success']);} ?>  
          </span>
        </h4>
         <h4>
          <span class="text text-danger"><?php 
          if ( isset($_SESSION['error']) ) {
          echo('<p>'.htmlentities($_SESSION['error'])."</p>\n");
          unset($_SESSION['error']);} ?>  
          </span>
        </h4>
        </div>

        <?php foreach($results as $result){ ?>
        <div class="col-md-4">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-warning">
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="<?= $result['picture']; ?>" alt="User Avatar">
                </div>
                <!-- /.widget-user-image -->
                <h3 class="widget-user-username"><?= $result['name']; ?></h3>
                <h5 class="widget-user-desc">Lead Developer</h5>
              </div>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <a href="profile.php?id=<?= $result['id']; ?>" class="nav-link">
                      <b>View Profile</b>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>

<?php }?>
         <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
</div>
  <!-- Control Sidebar -->

<?php include '../includes/sidfootscr.php'; ?>
</body>
</html>
