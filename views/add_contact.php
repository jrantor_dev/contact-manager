<?php 

session_start();
if(!isset($_SESSION['name'])){
  header("Location: login.php");
}

require "../vendor/autoload.php";
use App\Login;
use App\Contact;


 if(isset($_GET['status'])){
    if($_GET['status'] == 'logout') {
      Login::userLogout();
  }
}

$message = "";

if(isset($_POST['submit'])){
  if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['mobile']) && 
  !empty($_POST['fax']) && !empty($_POST['address']) && is_uploaded_file($_FILES['avatar']['tmp_name'])
){
    $message = Contact::add_contact($_POST);
  }
   
   else{
    $message = "All fields are required";
   }
}

$p_title = 'Add Contact';

?>

<!DOCTYPE html>
<html>

<?php include '../includes/header.php'; ?>

<body class="hold-transition sidebar-mini layout-fixed">
	<?php include '../includes/navbar.php'; ?>

	 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add Contact</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6 offset-3"> 
          <span class="text text-danger">
            <p><?php echo $message; ?></p>
          </span> 
           <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Contact</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="" method="post" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="number" name="phone" class="form-control" id="phone" placeholder="Phone">
                  </div>
                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="number" name="mobile" class="form-control" id="mobile" placeholder="Mobile">
                  </div>
                  <div class="form-group">
                    <label for="fax">Fax</label>
                    <input type="number" name="fax" class="form-control" id="phone" placeholder="Fax">
                  </div>
                  <div class="form-group">
                    <label for="Address">Address</label>
                    <textarea name="address" class="form-control" placeholder="Type Address"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="picture">Picture</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="picture" name="avatar">
                        <label class="custom-file-label" for="picture">Choose file</label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           </div> 
        </div>
      </div>
      </div>
    </div>

</div>

<?php include '../includes/sidfootscr.php'; ?>

</body>
</html>