<?php 

session_start();
if(!isset($_SESSION['name'])){
  header("Location: login.php");
}

require "../vendor/autoload.php";
use App\Login;
use App\Contact;


 if(isset($_GET['status'])){
    if($_GET['status'] == 'logout') {
      Login::userLogout();
  }
}

$message = "";

$id = $_GET['id'];
$contact = Contact::get_contact_by_id($id);

if(isset($_POST['submit'])){
  $message = Contact::update_contact($_POST);
}

$p_title = 'Edit Contact';

?>

<!DOCTYPE html>
<html>

<?php include '../includes/header.php'; ?>

<body>
	<?php include '../includes/navbar.php'; ?>

	 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
<!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit Contact</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-6 offset-3"> 
          <span class="text text-danger">
            <p><?php echo $message; ?></p>
          </span> 
           <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Contact</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="" method="post" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" value="<?php echo $contact['name']; ?>">
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" value="<?php echo $contact['email']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="number" name="phone" class="form-control" id="phone" value="<?php echo $contact['phone']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="number" name="mobile" class="form-control" id="mobile" value="<?php echo $contact['mobile']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="fax">Fax</label>
                    <input type="number" name="fax" class="form-control" id="fax" value="<?php echo $contact['fax']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="Address">Address</label>
                    <textarea name="address" class="form-control"><?php echo $contact['address']; ?></textarea>
                  </div>
                  <input type="hidden" name="c_id" value="<?php echo $contact['id'] ?>">
                  <div class="form-group">
                    <label for="picture">Picture</label>
                    <div class="input-group">
                      <img src="<?php echo $contact['picture']; ?>" height="50" width="50">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="picture" name="avatar">
                        <label class="custom-file-label" for="picture">Choose file</label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
           </div> 
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

  <?php include '../includes/sidfootscr.php'; ?> 
  </div>

</body>
</html>