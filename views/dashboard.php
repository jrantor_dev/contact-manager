<?php
session_start();
  if(!isset($_SESSION['name'])){
     header("Location: login.php");
}

require "../vendor/autoload.php";
use App\Login;
use App\Contact;
use App\Database;

 if(isset($_GET['status'])){
    if($_GET['status'] == 'logout') {
      Login::userLogout();
  }
}

if(isset($_GET['action'])){
  if($_GET['action'] == 'star'){
    $id = $_GET['id'];
     Contact::add_to_favorites($id);
  }

  elseif($_GET['action'] == 'unstar'){
    $id = $_GET['id'];
    Contact::remove_from_favorites($id);
  }
}



$p_title = 'Dashboard';

if(isset($_GET['page'])){
  $page = $_GET['page'];
} else{
  $page = 1;
}

$limit = 6;
$offset = ($page-1) * $limit; 

$pdo = Database::db_connect();
$stmt = $pdo->prepare("SELECT count(id) AS id FROM contacts WHERE user_id = :u_id");
$stmt->execute(array(
':u_id' => $_SESSION['user_id']
));

$q = $stmt->fetchAll();
$allrecords = $q[0]['id'];

$total_pages = ceil($allrecords/$limit);

$contacts = Contact::get_all_contacts($limit,$offset);

$prev = $page - 1;
$next = $page + 1;

?>

<!DOCTYPE html>
<html lang="en">

<?php include '../includes/header.php'; ?>

<body class="hold-transition sidebar-mini layout-fixed">

  <!-- Navbar -->
<?php include '../includes/navbar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-l-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6 offset-5">
            <?php if($allrecords<=0) { ?>
             <h4 >No Records Found.</h4>
            <?php } else{ ?>
          <h1 class="m-0 text-dark">All Contacts</h1>
          <?php } ?>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-sm-6 offset-4">  
        <h4>
          <span class="text text-success"><?php 
          if ( isset($_SESSION['success']) ) {
          echo('<p>'.htmlentities($_SESSION['success'])."</p>\n");
          unset($_SESSION['success']);} ?>  
          </span>
        </h4>
         <h4>
          <span class="text text-danger"><?php 
          if ( isset($_SESSION['error']) ) {
          echo('<p>'.htmlentities($_SESSION['error'])."</p>\n");
          unset($_SESSION['error']);} ?>  
          </span>
        </h4>
        </div>

      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
            <?php foreach($contacts as $contact) { ?>
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">

                <div class="card-header text-muted border-bottom-0">
                  Digital Strategist 
                  <?php if(Contact::isFav($contact['id'])) { ?>
                  <a href="dashboard.php?action=unstar&id=<?php echo $contact['id'];?>" 
                  class="btn btn-sm bg-yellow float-right">
                      <i class="fas fa-star"></i>
                    </a>
                  <?php }else{ ?>
                    <a href="dashboard.php?action=star&id=<?php echo $contact['id'];?>" 
                  class="btn btn-sm bg-white float-right">
                      <i class="fas fa-star"></i>
                    </a>
                    <?php } ?>
                  
                  
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      
                      <h2 class="lead"><b><?php echo $contact['name']; ?></b></h2>
                      <p class="text-muted text-sm"><b>About: </b> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: <?php echo $contact['address']; ?></li> <br>
                         <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span> Email #: 
                          <?php echo $contact['email']; ?>
                        </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: 
                          <?php echo $contact['phone']; ?>
                        </li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Mobile #: 
                          <?php echo $contact['mobile']; ?>
                        </li> <br>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-box"></i></span> Fax #: 
                          <?php echo $contact['fax']; ?>
                        </li>
                      </ul>
                    </div>
                    <div class="col-5 text-center">
                      <img src="<?php echo $contact['picture']; ?>" alt="" class="img-circle img-fluid">
                    </div>
                  </div>
                </div>
                <div class="card-footer">

                  <div class="text-right">
                    <a href="edit_contact.php?id=<?php echo $contact['id'];?>" class="btn btn-sm bg-teal">
                      <i class="fas fa-edit"></i>
                    </a>
                    <a href="profile.php?id=<?php echo $contact['id'];?>" class="btn btn-sm btn-primary">
                      <i class="fas fa-user"></i> View Profile
                    </a> 
                    <div class="text-left">
                      <a href="delete_contact.php?id=<?php echo $contact['id'];?>" class="btn btn-sm bg-red">
                      <i class="fas fa-trash"></i>
                    </a>
                    </div>
                    <p class="text-muted text-sm">Last Updated on <?php echo date("F j, Y g:ia", strtotime($contact['updated_at']));?></p>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
          </div>
        </div>
        <!-- /.card-body -->
        <?php if($total_pages > 0) { ?>
        <div class="card-footer">
          <nav aria-label="Contacts Page Navigation">
            <ul class="pagination justify-content-center m-0">
              <li class="page-item <?php if($page <= 1){ echo 'disabled'; } ?>">
                <a class="page-link" 
                href="<?php if($page <= 1){ echo '#'; } else { echo "?page=" . $prev; } ?>">Previous
              </a>
              </li>
              <?php for($i = 1; $i <= $total_pages; $i++){ ?>
              <li class="page-item <?php if($page==$i) echo 'active'; ?>">
                <a class="page-link" 
                  href="?page=<?php echo $i ?>">
                  <?= $i; ?>
                </a>

              </li>
            <?php } ?>
              <li class="page-item <?php if($page >= $total_pages) {echo 'disabled';} ?>"><a class="page-link" 
                href="<?php if($page >= $total_pages) {echo '#';} else{ echo "?page=".$next;} ?>">
              Next
            </a>
              </li>
            </ul>
          </nav>
        </div>
         <?php } ?>
        <!-- /.card-footer -->
      </div>

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
</div>
<?php include '../includes/sidfootscr.php'; ?>
</body>
</html>
