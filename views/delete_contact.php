<?php 

session_start();
  if(!isset($_SESSION['name'])){
     header("Location: login.php");
  }

require "../vendor/autoload.php";
use App\Login;
use App\Contact;


 if(isset($_GET['status'])){
    if($_GET['status'] == 'logout') {
      Login::userLogout();
  }
}

if(!isset($_GET['id'])){
	$_SESSION['error'] = "Missing parameter ID";
	header("Location: dashboard.php");
	return;
}

$id = $_GET['id'];
$contact = Contact::get_contact_by_id($id);

if(isset($_POST['delete'])){
	Contact::delete_contact($_POST['c_id']);
}

if(isset($_POST['cancel'])){
	header("Location: dashboard.php");
	return;
}

$p_title = 'Delete Contact';

?>

<!DOCTYPE html>
<html lang="en">

<?php include '../includes/header.php'; ?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
<?php include '../includes/navbar.php'; ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-l-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
              <li class="breadcrumb-item active">Delete Contact</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
         <div class="col-md-5 offset-3">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
              	<h4>Are You Sure To Delete?</h4>
                <h3 class="widget-user-username"><?php echo $contact['name'] ?></h3>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-1" src="<?php echo $contact['picture'] ?>" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="description-block">
		              <form action="" method="POST">
			          <input type="submit" name="cancel" class="btn btn-sm btn-primary" value="Cancel">
		              </form>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                  <div class="col-sm-6">
                    <div class="description-block">
                      <form action="" method="POST">
			          <input type="hidden" name="c_id" value="<?php echo $contact['id']; ?>">
			          <input type="submit" name="delete" class="btn btn-sm btn-danger" value="Delete">
		              </form>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
           <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->

<?php include '../includes/sidfootscr.php'; ?>

</body>
</html>