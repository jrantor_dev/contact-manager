<?php
session_start();
if(!isset($_SESSION['name'])){
	header("Location: login.php");
}

require "../vendor/autoload.php";
use App\Login;
use App\Contact;
use App\Profile;

 if(isset($_GET['status'])){
    if($_GET['status'] == 'logout') {
      Login::userLogout();
  }
}

if(isset($_POST['submit'])){
	if(!empty($_POST['profession']) && !empty($_POST['education']) && 
		!empty($_POST['about']) && !empty($_POST['notes'])){
 
 	  Profile::add_info($_POST);
  
   }

   else{
   	$_SESSION['error'] = "All fields are required";
   }

}

if(isset($_POST['edit'])){
	if(!empty($_POST['profession']) && !empty($_POST['education']) && 
		!empty($_POST['about']) && !empty($_POST['notes'])){
	Profile::edit_data($_POST);
  }
  else{
  	$_SESSION['error'] = "Fields cannot be empty";
  }
}

if(!isset($_GET['id'])){
	die("ID Parameter Missing!");
}

$id = isset($_GET['id']) ? $_GET['id'] : '';

$contact = Contact::get_contact_by_id($id);

$profile = Profile::get_data($id);


if(!$contact) die("Invalid ID Parameter");

$p_title = "Profile";

?>

<!DOCTYPE html>
<html>

<?php include '../includes/header.php'; ?>

<body class="hold-transition sidebar-mini layout-fixed">
	<?php include '../includes/navbar.php'; ?>

	<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
<!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item">Dashboard</li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div>

       <div class="col-sm-6 offset-4">  
        <h5>
          <span class="text text-success"><?php 
          if ( isset($_SESSION['success']) ) {
          echo('<p>'.htmlentities($_SESSION['success'])."</p>\n");
          unset($_SESSION['success']);} ?>  
          </span>
        </h5>
         <h5>
          <span class="text text-danger"><?php 
          if ( isset($_SESSION['error']) ) {
          echo('<p>'.htmlentities($_SESSION['error'])."</p>\n");
          unset($_SESSION['error']);} ?>  
          </span>
        </h5>
        </div>
          <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="<?= $contact['picture']; ?>"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?= $contact['name']; ?></h3>

                <p class="text-muted text-center"><?= $profile['profession'] ?></p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                   <p> Created at <?= 
                     date("F j, Y g:ia", strtotime($contact['created_at']));?></p>
                  </li>
                  <li class="list-group-item text text-center">
                    <a href="edit_contact.php?id=<?= $contact['id']; ?>"><b>Edit</b></a>
                  </li>
                  <li class="list-group-item text text-center">
                    <a href="delete_contact.php?id=<?= $contact['id']; ?>"><b>Delete</b></a>
                  </li>
                </ul>

                <a href="dashboard.php" class="btn btn-primary btn-block"><b>Dashboard</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
         
            <!-- /.card -->
          </div>

          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#about" data-toggle="tab">About</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  
                  <!-- /.tab-pane -->
                  <div class="active tab-pane" id="about">
                    <!-- The timeline -->
                    <strong><i class="fas fa-book mr-1"></i> Education</strong>

                <p class="text-muted">
                  <?= $profile['education']; ?>
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted"><?= $contact['address']; ?></p>

                <hr>

                <strong><i class="fas fa-envelope mr-1"></i> Contact</strong>

                <p class="text-muted"><?= $contact['email']; ?></p>
                <p class="text-muted"><?= $contact['mobile']; ?></p>
                <p class="text-muted"><?= $contact['phone']; ?></p>
                <p class="text-muted"><?= $contact['fax']; ?></p>

                <hr>

                <strong><i class="fa fa-user mr-1"></i> About</strong>
                <p class="text-muted"><?= $profile['about']; ?></p>
                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                <p class="text-muted"><?= $profile['notes']; ?></p>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                  	<?php if(!$profile) { ?>
                    <form class="form-horizontal" action="" method="POST">
                     <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Profession</label>
                        <div class="col-sm-10">
                          <input type="text" name="profession" class="form-control" id="inputProfession" placeholder="Profession">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Education</label>
                        <div class="col-sm-10">
                          <input type="text" name="education" class="form-control" id="inputEducation" placeholder="Education">
                          <input type="hidden" name="c_id" value="<?= $contact['id']; ?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">About</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="about" id="inputAbout" placeholder="About"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Notes</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="notes" id="inputNotes" placeholder="Notes"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </form>
                   <?php } else{?>
                   <form class="form-horizontal" action="" method="POST">
                     <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Profession</label>
                        <div class="col-sm-10">
                          <input type="text" name="profession" class="form-control" id="inputProfession" placeholder="Profession" value="<?= $profile['profession']; ?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Education</label>
                        <div class="col-sm-10">
                          <input type="text" name="education" class="form-control" id="inputEducation" placeholder="Education" value="<?= $profile['education']; ?>">
                          <input type="hidden" name="c_id" value="<?= $contact['id']; ?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">About</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="about" id="inputAbout" placeholder="About"><?= $profile['about']; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Notes</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="notes" id="inputNotes" placeholder="Notes"><?= $profile['notes']; ?></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" name="edit" class="btn btn-primary">Edit</button>
                        </div>
                      </div>
                    </form>

                    <?php }?> 
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
  <?php include '../includes/sidfootscr.php'; ?> 
  

</body>
</html>