<?php 
namespace App;
use App\Database;
use App\Validate;
use PDO;

class Contact{

	private static function image_handler(){
		$picture_name = $_FILES['avatar']['name'];
		$directory = '../assets/contacts/avatar/';
		$file = $directory.$picture_name;

		if(Validate::check_image($_FILES['avatar']['name'])){
			if($_FILES['avatar']['size'] < 1000000) {
				move_uploaded_file($_FILES['avatar']['tmp_name'], $file);
				return $file;
			}
			else{ die("File size exceeds required range.")  ;}
		}

		else{
			die("Use JPEG or PNG files only.");
		}
	}

	public static function add_contact($data){

		$picture = Contact::image_handler();

		if(Validate::check_name($data['name']) && Validate::check_email($data['email']) && Validate::check_mobile_num($data['mobile'])){

            $user_id = $_SESSION['user_id'];
			$name = $data['name'];
			$email = $data['email'];
			$phone = $data['phone'];
			$mobile = $data['mobile'];
			$fax = $data['fax'];
			$address = $data['address'];
			

			$pdo = Database::db_connect();
			$stmt = $pdo->prepare("INSERT INTO contacts (user_id,name,email,phone,mobile,fax,address,picture)
				VALUES (:u_id,:nm,:em,:ph,:mb,:fx,:addr,:pic)");

			$stmt->execute(array(
				':u_id' => $user_id,
				':nm' => $name,
				':em' => $email,
				':ph' => $phone,
				':mb' => $mobile,
				':fx' => $fax,
				':addr' => $address,
				':pic' => $picture
			));

			$_SESSION['success'] = "New Contact Added";
			header("Location: dashboard.php");
			return;
		}

		else{
			if(!Validate::check_name($data['name'])) $message = "Name format is not right.";
			if(!Validate::check_email($data['email'])) $message = "Email is not valid";
			if(!Validate::check_mobile_num($data['mobile'])) $message = "Mobile number is not valid";

			return $message;

		}

	}


	public static function get_all_contacts($limit,$offset){
		$pdo = Database::db_connect();
		$user_id = $_SESSION['user_id'];
		$stmt = $pdo->prepare("SELECT * FROM contacts WHERE user_id = :u_id ORDER BY id DESC LIMIT $offset,$limit");
		$stmt->execute(
			array(
			':u_id' => $user_id
		));

		return $stmt->fetchAll(PDO::FETCH_ASSOC);

	}

 
 public static function get_contact_by_id($id){
 	$user_id = $_SESSION['user_id'];
 	$pdo = Database::db_connect();

 	$stmt = $pdo->prepare("SELECT * FROM contacts WHERE id = :id AND user_id = :u_id");
 	$stmt->execute(array(
 		':id' => $id,
 		':u_id' => $user_id
	 ));
	 
 	return $stmt->fetch(PDO::FETCH_ASSOC);
 }

 public static function update_contact($data){
 	$img = $_FILES['avatar']['name'];
 	$pdo = Database::db_connect();

 	if(Validate::check_name($data['name']) && Validate::check_email($data['email']) && Validate::check_mobile_num($data['mobile'])){

 		    $id = $data['c_id'];
 		    $user_id = $_SESSION['user_id'];
			$name = $data['name'];
			$email = $data['email'];
			$phone = $data['phone'];
			$mobile = $data['mobile'];
			$fax = $data['fax'];
			$address = $data['address'];

			if($img){

				$stmt = $pdo->prepare("SELECT picture from contacts WHERE id = :id");
				$stmt->execute(array(
					':id' => $id
				));

				$c = $stmt->fetch(PDO::FETCH_ASSOC);
				unlink($c['picture']);

				$image = Contact::image_handler();
				$q = "UPDATE contacts SET name = :nm, email = :em, phone = :ph, mobile = :mb, fax = :fx, address = :addr, picture = :pc WHERE id = :id AND user_id = :u_id";

				$stmt1 = $pdo->prepare($q);
				$stmt1->execute(array(
				':id' => $id,
				':u_id' => $user_id,
				':nm' => $name,
				':em' => $email,
				':ph' => $phone,
				':mb' => $mobile,
				':fx' => $fax,
				':addr' => $address,
				':pc' => $image
				));

				$_SESSION['success'] = "Contact Info Updated.";
				header("Location: dashboard.php");
				return;
			}

			else{
				$query = "UPDATE contacts SET name = :nm, email = :em, phone = :ph, mobile = :mb, fax = :fx, address = :addr WHERE id = :id AND user_id = :u_id";
				$stmt2 = $pdo->prepare($query);
				$stmt2->execute(array(
				':id' => $id,
				':u_id' => $user_id,
				':nm' => $name,
				':em' => $email,
				':ph' => $phone,
				':mb' => $mobile,
				':fx' => $fax,
				':addr' => $address
				));

				$_SESSION['success'] = "Contact Info Updated.";
				$stmt = null;
	            $pdo = null;
				header("Location: dashboard.php");
				return;
			}
 	}

 	else{

 		if(!Validate::check_name($data['name'])) $message = "Name format is not right.";
		if(!Validate::check_email($data['email'])) $message = "Email is not valid";
		if(!Validate::check_mobile_num($data['mobile'])) $message = "Mobile number is not valid";

		return $message;
 	}
 }


 public static function delete_contact($id){
 	$pdo = Database::db_connect();
 	$st = $pdo->prepare("SELECT picture FROM contacts WHERE id = :id AND user_id = :u_id");
 	$st->execute(array(
 		':id' => $id,
 		':u_id' => $_SESSION['user_id']
 	));

 	$contact = $st->fetch(PDO::FETCH_ASSOC);
 	unlink($contact['picture']);

 	$sql = "DELETE FROM contacts WHERE id = :id AND user_id = :u_id";
	$stmt = $pdo->prepare($sql);
	$stmt->execute(array(
		':id' => $id,
		':u_id' => $_SESSION['user_id']
	));

	$_SESSION['success'] = 'Record deleted';
	$stmt = null;
	$pdo = null;
    header( 'Location: dashboard.php' ) ;
    return;
 }

 public static function search_contacts(){
 	$pdo = Database::db_connect();
	$user_id = $_SESSION['user_id'];
	$stmt = $pdo->prepare("SELECT * FROM contacts WHERE user_id = :u_id ORDER BY id DESC");
	$stmt->execute(
		array(
		':u_id' => $user_id
	));
	$q = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$stmt = null;
	$pdo = null;

	return $q;
 }


 public static function add_to_favorites($id){
	 $pdo = Database::db_connect();
	 $u_id = $_SESSION['user_id'];

	 $stmt = $pdo->prepare("INSERT INTO favorites (user_id,fav_id) VALUES (:u_id, :fav_id)");

	 $stmt->execute(array(
		 ':u_id' => $u_id,
		 ':fav_id' => $id
	 ));

	 $_SESSION['success'] = "Added to Favorites!";
	 $stmt = null;
	 $pdo = null;
	 header("Location: dashboard.php");
	 
	 exit();
 }

 public static function remove_from_favorites($id){
	 $pdo = Database::db_connect();
	 $u_id = $_SESSION['user_id'];

	 $stmt = $pdo->prepare("DELETE FROM favorites WHERE user_id = :u_id AND fav_id = :id");

	 $stmt->execute(array(
		 ':u_id' => $u_id,
		 ':id' => $id
	 ));

	 $_SESSION['success'] = "Removed from Favorites!";
	 $stmt = null;
	 $pdo = null;
	 header("Location: dashboard.php");
	 exit();
 }


 public static function isFav($id){
	 $pdo = Database::db_connect();
	 $u_id = $_SESSION['user_id'];
	 $stmt = $pdo->prepare("SELECT user_id,fav_id FROM favorites WHERE user_id = :u_id AND fav_id = :f_id");

	 $stmt->execute(array(
		 ':u_id' => $u_id,
		 ':f_id' => $id
	 ));

	 $q = $stmt->fetchAll();

	 $stmt = null;
	 $pdo = null;

	foreach($q as $v){
		return $v['fav_id'] == $id;
	}
 }
 

}

?>