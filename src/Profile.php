<?php
namespace App;
use App\Database;
use App\Validate;
use PDO;

class Profile{


	public static function add_info($data){

		$contact_id = $data['c_id'];
		$user_id = $_SESSION['user_id'];
 		$profession = Validate::input_check($data['profession']);
		$education = Validate::input_check($data['education']);
		$notes = Validate::input_check($data['notes']);
		$about = Validate::input_check($data['about']);

		$pdo = Database::db_connect();
		$query = "INSERT INTO profile (contact_id,user_id,profession,education,about,notes) VALUES(:c_id,:u_id,:pr,:ed,:ab,:nt)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array(
			':c_id' => $contact_id,
			':u_id' => $user_id,
			':pr' => $profession,
			':ed' => $education,
			':ab' => $about,
			':nt' => $notes
		));

		$_SESSION['success'] = "Profile Info Added.";
		header("Location: profile.php"."?id=".$contact_id);
		return;
	}

	public static function get_data($id){

		$user_id = $_SESSION['user_id'];
		$pdo = Database::db_connect();
		$stmt = $pdo->prepare("SELECT * FROM profile WHERE contact_id = :c_id AND user_id = :u_id");
		$stmt->execute(array(
			':c_id' => $id,
			':u_id' => $user_id
		));

		return $stmt->fetch(PDO::FETCH_ASSOC);

	}

	public static function edit_data($data){
		$contact_id = $data['c_id'];
		$user_id = $_SESSION['user_id'];
 		$profession = Validate::input_check($data['profession']);
		$education = Validate::input_check($data['education']);
		$notes = Validate::input_check($data['notes']);
		$about = Validate::input_check($data['about']);

		$pdo = Database::db_connect();
		$stmt = $pdo->prepare("UPDATE profile SET profession = :pf, education = :ed, about = :ab, notes = :nt WHERE contact_id = :c_id AND user_id = :u_id");
		$stmt->execute(array(
			':c_id' => $contact_id,
			':u_id' => $user_id,
			':pf' => $profession,
			':ed' => $education,
			':ab' => $about,
			':nt' => $notes
		));

		$_SESSION['success'] = "Profile Updated.";
		header("Location: profile.php"."?id=".$contact_id);
		return;
	}
}


?>