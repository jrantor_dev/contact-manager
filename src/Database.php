<?php

namespace App;
use PDO;

/**
 * 
 */
class Database
{
	
	public function db_connect()
	{
		$pdo = new PDO('mysql:host=localhost;port=3306;dbname=contacts', 'root', '');
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $pdo;
	}

	public function db_close(){
		$this->$pdo = null;
	}
}

?>