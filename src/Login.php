<?php

namespace App;
use App\Database;
use App\Validate;
use PDO;
/**
 * 
 */
class Login
{
	public static function register($data){

	if(Validate::check_name($data['name']) && Validate::check_email($data['email']) && Validate::check_password($data['password'],$data['password_confirmation']) ){

	$fullname = $data['name'];
	$email = $data['email'];
	$password = hash('md5',$data['password']);

	$pdo = Database::db_connect();

	$stmt = $pdo->prepare('INSERT INTO users (fullname,email,password) VALUES (:fn,:em,:ps)');

	$stmt->execute(array(

	':fn' => $fullname,
	':em' => $email,
	':ps' => $password

	));
	$_SESSION['success'] = "Welcome User ".$fullname;
	$_SESSION['name'] = $fullname;
	$_SESSION['id'] = $pdo->lastInsertId();
		header('Location: views/dashboard.php');
		return;
   }

   else{


    if(!Validate::check_name($data['name']))  $message = 'Name format is not right!';

    if(!Validate::check_email($data['email']))  $message = 'Email format is not right!';

    if(!Validate::check_password($data['password'], $data['password_confirmation']))  $message = 'Password did not match!';

    return $message;
   }
  
  }


public static function userLogin($data){
	if(!empty($data['email']) && !empty($data['password'])){
		$email = $data['email'];
		$password = hash('md5',$data['password']);
		$pdo = Database::db_connect();
		$stmt = $pdo->prepare("SELECT id,fullname,email,password FROM users WHERE email = :em AND password = :ps");
		$stmt->execute(array(
			':em' => $email,
			':ps' => $password
		));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if($row){
			session_start();
			$_SESSION['name'] = $row['fullname'];
			$_SESSION['user_id'] = $row['id'];
			header("Location: dashboard.php");
			exit;
		}
		else{
			$_SESSION['error'] = "Invalid email and password combination.";
			return;
		}
	}

	else{
		$_SESSION['error'] = "Email and password can not be empty.";
		return;
	}
}

  public static function userLogout(){
  	unset($_SESSION['name']);
  	unset($_SESSION['user_id']);
  	header("Location: login.php");
  	return;
  }


}




?>