<?php 
namespace App;

class Validate {

	public function input_check($data){
		$data = htmlentities($data);
		return $data;
	 }

	 public function check_email($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	 }

	 public function check_password($password,$confirmation){
	 	return $password == $confirmation;
	 }

	 public function check_name($name){
	 	return preg_match('/^[a-zA-Z\040]+$/',$name);
	 }

	 public function check_mobile_num($number){
	 	return preg_match('/^01[3-9]{1}[\d]{2}[\d]{6}$/',$number);
	 }

	public function check_image($file){
		$file_type = pathinfo($file, PATHINFO_EXTENSION);
		return $file_type == 'jpg' || $file_type == 'png' || $file_type == 'jpeg';
	}

}

?>